import java.util.Random;
import java.util.stream.Stream;

public class Generatefunction {
    public static void main(String []args){
        Stream.generate(new Random()::nextInt)
                .limit(5).forEach(System.out::println);
    }
}
