

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

import static java.util.Collections.replaceAll;

public class ReadFile {
    void readline(Path path){
        try {
            Stream<String> lines = Files.lines(path);
            lines.forEach(s->System.out.println(s));
            lines.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void main(String []args) {
        // File file = new File("/Users/admin/Documents/Measurementclass.rtf");
        Path path = Paths.get("/Users/admin/Downloads/Summary09Apr2021.rtf");
        ReadFile r = new ReadFile();
        r.readline(path);
    }

}