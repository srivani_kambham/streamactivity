import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.nio.file.Files.*;

public class ReadFileTest {
    private Path workingDir;


    @Test
    public void read()  {
        ReadFile readfile = new ReadFile();
        Path file = Paths.get("/Users/admin/Downloads/Summary09Apr2021.rtf");
        readfile.readline(file);
        Assertions.assertNotNull(readfile);
    }

}
